package com.github.wxiaoqi.security.homSys.rest;

import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.dahua.openapi.util.MySecureProtocolSocketFactory;
import com.github.wxiaoqi.security.common.msg.ObjectRestResponse;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.commons.httpclient.protocol.ProtocolSocketFactory;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.InputStream;
import java.util.*;

/**
 * 乐橙的获取视频地址
 */
@Controller
@RequestMapping("petVideo")
public class PetVideoController {

    private static String PORT = "443";
    private static String URL = "https://openapi.lechange.cn";
    // 如果不知道appid，请登录open.lechange.com，开发者服务模块中创建应用
    private static String APPID = "lc415e62c1b72842e6";
    // 如果不知道appsecret，请登录open.lechange.com，开发者服务模块中创建应用
    private static String SECRET = "27f0d7b6fdc047d8a2d1a827b70039";

    private static String DEVICE_OFFLINE_STATUS = "0";// 离线
    private static String DEVICE_ONLINE_STATUS = "1";// 在线

    @RequestMapping(value = "/videourl", method = RequestMethod.POST)
    @ResponseBody
    public ObjectRestResponse getVideoUrl() {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        List<JSONObject> resultList = new ArrayList<>();
        JSONObject json = new JSONObject();
        json = execute(paramsMap,"accessToken");
        JSONObject jsonResult = json.getJSONObject("result");
        JSONObject jsonData = jsonResult.getJSONObject("data");
        String token = jsonData.getString("accessToken");
        System.out.println(token);

        paramsMap = new HashMap<String, Object>();
        paramsMap.put("token",token);
        paramsMap.put("queryRange","1-99");
        json = execute(paramsMap, "liveList");
        System.out.println(json);
        if (!JSONUtil.isNull(json)){
            JSONArray deviceLisrtStreams = json.getJSONObject("result").getJSONObject("data").getJSONArray("lives");
            for (Object stream : deviceLisrtStreams) {
                JSONObject deviceStream = (JSONObject) stream;
                resultList.add((JSONObject) deviceStream.getJSONArray("streams").get(0));
            }
            return new ObjectRestResponse().data(resultList).rel(true);
        }

//        System.out.println(json);
//        if (!JSONUtil.isNull(json)){
//            JSONArray devicesArray = json.getJSONObject("result").getJSONObject("data").getJSONArray("devices");
//            if (!JSONUtil.isNull(devicesArray)){
//                for (Object deviceObj : devicesArray) {
//                    JSONObject device = (JSONObject) deviceObj;
//                    String deviceId = device.getString("deviceId");
//                    String deviceStatus = device.getString("status");
//                    if (StringUtils.hasLength(deviceId) && DEVICE_ONLINE_STATUS.equals(deviceStatus)){
//                        paramsMap = new HashMap<String, Object>();
//                        paramsMap.put("deviceId",deviceId);
//                        paramsMap.put("token",token);
//                        paramsMap.put("channelId","0");
//                        paramsMap.put("streamId",0);
//                        paramsMap.put("liveMode","proxy");
//                        json = execute(paramsMap, "bindDeviceLiveHttps");
//                        JSONArray streams = json.getJSONObject("result").getJSONObject("data").getJSONArray("streams");
//                        resultList.add((JSONObject) streams.get(0));
//                        System.out.println(json);
//                    }
//                }
//            }
//            System.out.println("结果：" + resultList.toString());
//            return new ObjectRestResponse().data(resultList).rel(true);
//        }
        return new ObjectRestResponse().rel(false);
    }


    public static JSONObject execute(Map<String, Object> paramsMap, String method) {
        Map<String, Object> map = paramsInit(paramsMap);
        // 返回json
        JSONObject jsonObj = doPost(URL + ":" + PORT + "/openapi/" + method, map);
        System.out.println("=============================");
        System.out.println("返回结果：" + jsonObj.toJSONString());
        return jsonObj;

    }

    public static JSONObject doPost(String url, Map<String, Object> map) {
        String json = JSON.toJSONString(map);
        ProtocolSocketFactory factory = new MySecureProtocolSocketFactory();
        Protocol.registerProtocol("https", new Protocol("https", factory, 443));
        HttpClient client = new HttpClient();
        client.getParams().setParameter(HttpMethodParams.HTTP_CONTENT_CHARSET, "UTF-8");
        PostMethod method = new PostMethod(url);
        System.out.println(url);
        JSONObject jsonObject = new JSONObject();
        try {
            RequestEntity entity = new StringRequestEntity(json, "application/json", "UTF-8");
            method.setRequestEntity(entity);
            client.executeMethod(method);

            InputStream inputStream = method.getResponseBodyAsStream();
            String result = IOUtils.toString(inputStream, "UTF-8");
            jsonObject = JSONObject.parseObject(result);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            method.releaseConnection();
        }
        return jsonObject;
    }

    protected static Map<String, Object> paramsInit(Map<String, Object> paramsMap) {
        long time = System.currentTimeMillis() / 1000;
        String nonce = UUID.randomUUID().toString();
        String id = UUID.randomUUID().toString();

        StringBuilder paramString = new StringBuilder();
        paramString.append("time:").append(time).append(",");
        paramString.append("nonce:").append(nonce).append(",");
        paramString.append("appSecret:").append(SECRET);

        String sign = "";
        // 计算MD5得值
        try {
            System.out.println("传入参数：" + paramString.toString().trim());
            sign = DigestUtils.md5Hex(paramString.toString().trim().getBytes("UTF-8"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        Map<String, Object> systemMap = new HashMap<String, Object>();
        systemMap.put("ver", "1.0");
        systemMap.put("sign", sign);
        systemMap.put("appId", APPID);
        systemMap.put("nonce", nonce);
        systemMap.put("time", time);

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("system", systemMap);
        map.put("params", paramsMap);
        map.put("id", id);
        return map;
    }

}
